# FOR EACH VIDEO, COUNT COMMENTS.
# RAW DATA HAS 1 COMMENT PER LINE.
# 
# STEP 1. MAP each line, output video id and 1 for this comment

# open data.csv for reading as a file stream named i
# open m.csv for writing as a file stream named o
i=open("data.csv","r")
o=open("m.csv","w")


# for each line in your input file stream
for line in i:


  # strip the line, split it by the delimiter into a list named 'data'
  datalist=line.strip().split(",")

  # if len(data) is equal to 4 (remember the colon)
  if len(data)==4:
    video_id,comment_text,likes,replies=datalist
    o.write(str(video_id),",1\n")
  

    # assign data to four named variables
    
    
    # use o.write to output the id comma 1
i.close()
o.close()

# close your file streams

